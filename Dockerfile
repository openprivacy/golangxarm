FROM golang:1.21.5

RUN apt-get update \
	&& apt-get install -y gcc-aarch64-linux-gnu \
	&& rm -rf /var/lib/apt/lists/*
