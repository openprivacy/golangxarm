# golangxarm

**Deprecated:** in favor of [go-cross-compile](https://git.openprivacy.ca/openprivacy/go-cross-compile) at [dockerhub/openpriv/go-cross-compile](https://hub.docker.com/r/openpriv/go-cross-compile)

golang docker container with compilers to cross compile for arm
